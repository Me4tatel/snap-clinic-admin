import { combineReducers } from 'redux';
import Symptoms from './symptoms.js';
import Concerns from './concerns.js';
import { routerReducer } from 'react-router-redux';
import search from './search.js';

const  allReducers = combineReducers ({
	symptoms: Symptoms,
	concerns: Concerns,
	routing: routerReducer,
	search: search, 
});

export default allReducers
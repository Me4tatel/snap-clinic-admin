import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';

import Content from '../content/';

import { connect } from 'react-redux';
import Redirect from "react-router/es/Redirect";


class app extends Component{
	render(){
		return (
			<div>
				<Switch>
					<Route exact path="/:page" render={ (match) => {
						return(
							<Content match={this.props.match} />
 						)}
					}/>
					<Route component={() => <Redirect to="/concerns" />}/>
				</Switch>
			</div>	
		);
	}

}

export default withRouter(connect(
	(state, ownProps) => ({
		store: state,
		ownProps
	}))(app)
);
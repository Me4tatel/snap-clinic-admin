import React, {Component} from "react";
import { sortable } from 'react-sortable';


class Item extends Component {
    render() {
        let {
            item,
            isDeleted,
            checkSiblings,
            removeItem,
            openModal,
            draggable,
            onDragEnd,
            onDragOver,
            onTouchStart,
            onTouchMove,
            onTouchEnd,
            onDragStart,
        } = this.props;

        return (
            <li
                data-id={this.props['data-id']}
                draggable={draggable && this.props.page === "concerns"}
                onDragEnd={onDragEnd}
                onDragOver={onDragOver}
                onTouchStart={onTouchStart}
                onTouchMove={onTouchMove}
                onTouchEnd={onTouchEnd}
                onDragStart={onDragStart}
                className={'list-item' + (isDeleted?' delete': '') + (item.id === this.props.choosedItem && this.props.page === "concerns" ? ' active': '')}
                key={item.id}>
                <div className="left">
                    {this.props.page === "concerns"?<div className="icon-move">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.25 21">
                            <circle className="cls-1" cx="9.87" cy="6.74" r="1.87" />
                            <circle className="cls-1" cx="9.87" cy="14.38" r="1.87" />
                            <circle className="cls-1" cx="2.37" cy="2.37" r="1.87" />
                            <circle className="cls-1" cx="2.37" cy="10.5" r="1.87" />
                            <circle className="cls-1" cx="2.37" cy="18.63" r="1.87" />
                        </svg>
                    </div>:''}
                    <span onClick={this.props.page === "concerns"?()=>{checkSiblings(item.id)}:()=>(0)} style={this.props.page === "concerns"?{cursor:'pointer'}:{}}>{item.name ? item.name: item.text }</span>
                </div>
                <div className="right">
                    <span className="conroll-btn" onClick={()=>{openModal(item.name ? item.name: item.text, item.id)}}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19.01">
                            <polygon className="cls-1" points="17.53 4.88 4.92 17.49 0.5 18.51 1.52 14.08 14.13 1.48 17.53 4.88" />
                            <path className="cls-1" d="M1.52,14.1A4.4,4.4,0,0,1,3.7,15.31a4.48,4.48,0,0,1,1.21,2.18" />
                            <path className="cls-1" d="M.8,17.22a1.19,1.19,0,0,1,.65.33,1.28,1.28,0,0,1,.36.65" />
                            <path className="cls-1" d="M15.42-.21h.38a2.4,2.4,0,0,1,2.4,2.4v0a2.4,2.4,0,0,1-2.4,2.4h-.38a0,0,0,0,1,0,0V-.21a0,0,0,0,1,0,0Z" transform="translate(3.38 12.53) rotate(-45)" />
                        </svg>
                    </span>
                    {this.props.page === "concerns" ? <span className="conroll-btn" onClick={()=>{checkSiblings(item.id)}}>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 18.41">
                                    <polyline className="cls-1" points="2.07 14.77 2.07 9.1 18.93 9.1 18.93 14.77" />
                                    <line className="cls-1" x1="10.5" y1="14.77" x2="10.5" y2="0.5" />
                                    <circle className="cls-1" cx="2.07" cy="16.34" r="1.57" />
                                    <circle className="cls-1" cx="10.5" cy="16.34" r="1.57" />
                                    <circle className="cls-1" cx="18.93" cy="16.34" r="1.57" />
                                </svg>
                            </span>:''}
                    <span className="conroll-btn" onClick={()=>{removeItem(item.id)}}>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.03 19.88">
                                    <line className="cls-1" x1="0.5" y1="19.38" x2="19.38" y2="0.5" />
                                    <line className="cls-1" x1="19.53" y1="19.38" x2="0.65" y2="0.5" />
                                </svg>
                            </span>
                    <div className="back-action">This element was deleted. <div className="make" onClick={()=>(this.props.cancelRemove(item.id))}>Undo</div></div>
                </div>
            </li>
        )
    };
}
let SortableItem = sortable(Item);

export default SortableItem;
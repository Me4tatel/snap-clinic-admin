const dataTrasporter = require('../core/data-trasporter.js').default;

export const getConcerns = () => { 
	return dataTrasporter.get('Concerns')
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'DOWNLOAD_CONCERNS',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const addConcern = (data) =>{
	return dataTrasporter.post(`Concerns?name=${data.name}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'ADD_CONCERN',
				payload: result.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const changeConcern = (data) =>{
	return dataTrasporter.put(`Concerns/${data.id}?name=${data.name}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'CHANGE_CONCERN',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const deleteConcern = (id) =>{
	return dataTrasporter.delete(`Concerns/${id}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			if(result.data.isSuccess){
				return {
					type: 'REMOVE_CONCERN',
					payload: {id}
				};
			}
		})
		.catch(error=>{
			console.log(error);
		});
}

export const changeOrderConcern = (list) =>{
    let order = 1;
    let sendedData = [];
    list.forEach((item)=>{
        sendedData.push({
            id: item,
            order: order
        });
        order++;
    });
    return dataTrasporter.put(`Concerns/api/Concerns/UpdateOrder`, sendedData)
        .then(result=>{
            if(result.error){
                throw new Error("Error in downloading objects");
            }
            if(result.data.isSuccess){
                return {
                    type: 'CHANGE_ORDER',
                    payload: {list}
                };
            }
        })
        .catch(error=>{
            console.log(error);
        });
}


export const getConcernSymptoms = (id) =>{
	return dataTrasporter.get(`ConcernSymptoms/${id}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in downloading objects");
			}
			return {
				type: 'DOWNLOAD_CONCERN_SYMPTOMS',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const addConcernSymptom = (concernId, symptomId) =>{
    return dataTrasporter.post(`ConcernSymptoms/${concernId}?symptomId=${symptomId}`)
        .then(result=>{
        	console.log(result);
            if(result.error){
                throw new Error("Error in removing objects");
            }
            return {
                type: 'ADD_CONCERN_SYMPTOM',
                payload: result.data.data
            };
        })
        .catch(error=>{
            console.log(error);
        });
}

export const removeConcernSymptom = (concernId, symptomId) =>{
	return dataTrasporter.delete(`ConcernSymptoms/${concernId}?symptomId=${symptomId}`)
		.then(result=>{
			if(result.error){
				throw new Error("Error in removing objects");
			}
			return {
				type: 'REMOVE_CONCERN_SYMPTOM',
				payload: result.data.data
			};
		})
		.catch(error=>{
			console.log(error);
		});
}

export const changeOrderConcernSymptoms = (concernId, orders) =>{
	let order = 1;

	let sendedData = {
		concernId: concernId,
		symptomsOrder: []
	};
	let arrFunctions = [];
    orders.forEach((item)=>{
    	sendedData.symptomsOrder.push({
            symptomId: item,
            order: order
		});
        order++;
	});
    return Promise.all(arrFunctions).then(() => {
        return dataTrasporter.put(`ConcernSymptoms`, sendedData, {
            headers: {
                "Content-Type": "application/json-patch+json"
            }
        })
            .then(result=>{
                if(result.error){
                    throw new Error("Error in downloading objects");
                }
                return {
                    type: 'DOWNLOAD_CONCERN_SYMPTOMS',
                    payload: result.data.data
                };
            })
            .catch(error=>{
                console.log(error);
            });
    });
}
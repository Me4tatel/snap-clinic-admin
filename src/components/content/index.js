import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import LeftSidebar from './left-sidebar/';
import RightContent from './right-content/';
import Modal from './Modal/';

import { connect } from 'react-redux';


class app extends Component{
	constructor(props) {
        super(props);
        this.state = {
			modalShow: false,
			modalContent:{
				text:'',
				elements:[]
			},
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
	}
	openModal(content){
		this.setState({
			modalShow:true,
			modalContent:content
		})
	}
	closeModal(){
		this.setState({
			modalShow:false,
		})
	}
	render(){
		return (
			<main>
				<LeftSidebar match={this.props.match}/>
				<RightContent match={this.props.match} openModal={this.openModal} closeModal={this.closeModal} stateModal={this.state.modalShow}/>
				<Modal match={this.props.match} key={JSON.stringify(this.state.modalContent)} content={this.state.modalContent} show={this.state.modalShow} close={this.closeModal}/>
			</main>	
		);
	}

}

export default withRouter(connect(
	(state, ownProps) => ({
		store: state,
		ownProps
	}))(app)
);
import React, { Component } from 'react';
import withRouter from "react-router/es/withRouter";
import { connect } from 'react-redux';

import { addConcern, changeConcern } from '../../../view/js/actions/concerns.js';
import { addSymptom, changeSymptom } from '../../../view/js/actions/symptoms.js';

class Modal extends Component{
    constructor(props){
        super(props);
        this.state = {
            answers:{}
        };
        this.setAnswers = this.setAnswers.bind(this);
    }	
    controllElement(element){
        switch (element.type) {
            case 'input': {
                if(typeof this.state.answers[element.name] === "undefined"){
                    let answers = Object.assign({}, this.state.answers);
                    answers[element.name] = element.value?element.value:'';
                    this.setState({
                        answers:answers
                    });
                }
                return(<input type="text" required placeholder={element.placeholder} name={element.name} defaultValue={element.value} onChange={this.setAnswers}/>
            );
            }
            case 'select': {
                if (typeof this.state.answers[element.name] === "undefined") {
                    let answers = Object.assign({}, this.state.answers);
                    answers[element.name] = element.value?element.value:'';
                    this.setState({
                        answers: answers
                    });
                }
                return (
                    <select name={element.name} defaultValue={element.value?element.value:''} required onChange={this.setAnswers}>
                        <option hidden disabled value="">{element.placeholder}</option>
                        <option value="CheckBox">CheckBox</option>
                        <option value="RadioButton">RadioButton</option>
                        <option value="Scroller">Scroller</option>
                        <option value="Textarea">Textarea</option>
                        <option value="Textbox">textbox</option>
                    </select>
                )
            }
            default:{
                if(typeof this.state.answers[element.name] === "undefined"){
                    let answers = Object.assign({}, this.state.answers);
                    answers[element.name] = element.value;
                    this.setState({
                        answers:answers
                    });
                }
                return(<input type="text" required placeholder={element.placeholder} name={element.name} defaultValue={element.value} onChange={this.setAnswers}/>)
            }
        }
        return false;
    }
    sendData(event, make){
        event.preventDefault();
        let { addConcern, changeConcern, addSymptom, changeSymptom, close } = this.props;
        console.log(this.state.answers);
        switch(make){
            case 'changeConcern':
                changeConcern({
                    ...this.state.answers,
                    id: this.props.content.id
                });
                close();
            break;
            case 'createConcern':
                addConcern(this.state.answers);
                close();
            break;
            case 'changeSymptom':
                changeSymptom({
                    ...this.state.answers,
                    id: this.props.content.id
                });
                close();
            break;
            case 'createSymptom':
                addSymptom(this.state.answers);
                close();
            break;
            default: {
                addConcern(this.state.answers);
                close();
            }
        }

    }

    setAnswers(e){
        let answers = Object.assign({}, this.state.answers)
        answers[e.target.name] = e.target.value;
        this.setState({
            answers:answers
        });
    }

	render(){
        let {content, show, close} = this.props;
		return (
            <div className={'modal-window' + (show ? ' show' : '')}>
                <div className="modal-content">
                    <h4>{content.text}</h4>
                    <form onSubmit={(e)=>(this.sendData(e, content.do))}>
                        {content.elements && content.elements.map((element)=>(
                            <div className="field" key={element.name+element.type}>
                                <span>
                                    {element.title[0].toUpperCase() + element.title.substring(1)} {element.require?<i className="red">*</i>:''}
                                </span>
                                {this.controllElement(element)}
                            </div>
                        ))}
                        <div className="center">
                            <button className="btn">Save</button>
                        </div>
                    </form>
                </div>
                <div className="modal-overlay" onClick={close}>
                </div>
            </div>			
        );
	}
}

export default withRouter(connect(
    (state, ownProps) => {
        return{
            ownProps
        }
    },
    dispatch => ({
        addConcern: (data) => {
            dispatch(addConcern(data));
        },
        addSymptom: (data) => {
            dispatch(addSymptom(data));
        },
        changeConcern: (data) => {
            dispatch(changeConcern(data));
        },
        changeSymptom: (data) => {
            dispatch(changeSymptom(data));
        }
    })
)(Modal));
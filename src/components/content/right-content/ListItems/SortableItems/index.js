import React, {Component} from "react";
import SortableItem from "./SortableItem";


class SortableItems extends Component {
    constructor(props){
        super(props);
        this.state = {
            items: this.props.items
        }
    }

    onSortItems = (items) => {
        console.log(items);
        this.setState({
            items: items
        });
        let orders = items.map((item)=>{
            return item.id;
        });
        this.props.changeOrder(orders);
    };
    render() {
        let { items, listRemove, removeItem, cancelRemove, checkSiblings, choosedItem, openModal, page } = this.props;
        let listItems = items && items.map((item, i) => {
            return(<SortableItem
                item={item}
                key={item.id}
                onSortItems={this.onSortItems}
                sortId={i}
                choosedItem={choosedItem}
                items={items}
                page={page}
                removeItem={removeItem}
                cancelRemove={cancelRemove}
                openModal={openModal}
                checkSiblings={checkSiblings}
                isDeleted={!!listRemove[item.id]}
            />
        )});
        return (<ul className="sortable-list list-items">
            {listItems}
        </ul>);
    }
}

export default SortableItems;
import React, { Component } from 'react';
import withRouter from "react-router/es/withRouter";
import { connect } from 'react-redux';
import { findConcerns, findSymptoms } from "../../../../view/js/actions/search.js";

import { removeConcernSymptom, addConcernSymptom, changeOrderConcernSymptoms } from '../../../../view/js/actions/concerns.js';
import { removeSymptomConcern } from '../../../../view/js/actions/symptoms.js';
import iconSearch from '../../../../view/img/icons/icon-search.svg'

import SortableItems from './SortableItems';
import CustomScroll from "react-custom-scroll";

class BlockSiblings extends Component{
    constructor(props){
        super(props);
        this.state = {
            forRemove:{}
        }
        this.findItems = this.findItems.bind(this);
        this.changeOrder = this.changeOrder.bind(this);
    }

    findItems(e){
        this.props.page === "concerns" ? this.props.onFindSymptoms(e.target.value) : this.props.onFindConcerns(e.target.value);
    }
    changeOrder(orders){
        this.props.changeOrderConcernSymptoms(this.props.parentId, orders)
    }

    tryRemove(parentId, childId){
        let { forRemove } = this.state;
        let newForRemove = {...forRemove};
        newForRemove[parentId] = newForRemove[parentId] === undefined ? {} : newForRemove[parentId];
        newForRemove[parentId][childId] = setTimeout(()=>{
            this.props.removeConcernSymptom(parentId, childId);
            let { forRemove } = this.state;
            let newForRemove = {...forRemove};
            delete newForRemove[parentId][childId];
            this.setState({
                forRemove: newForRemove
            });
        }, 10000);
        this.setState({
            forRemove: newForRemove,
        })
    }

    cancelRemove(parentId, childId){
        let { forRemove } = this.state;
        let newForRemove = {...forRemove};
        clearTimeout(newForRemove[parentId][childId]);
        delete newForRemove[parentId][childId];
        this.setState({
            forRemove: newForRemove
        })
    }

	render(){
        let { items, itemsId, parentId } = this.props;
        let { forRemove } = this.state;
        let searchedItems = this.props.page === "concerns" ? this.props.symptomsSearched : this.props.concernsSearched;
        let filteredSearchedItems = searchedItems.filter(item => !itemsId.includes(item.id));
        return (
            <div className="block-siblings">
                <div className="top">
                    <div className="wrapper-input">
                        <img src={iconSearch} alt="" className="icon-search" />
                        <input type="text" placeholder="To connect a symptom start typing its title in here" onChange={this.findItems}/>
                        <ul className="search-result">
                            <CustomScroll heightRelativeToParent="100%">
                            {
                                filteredSearchedItems.length ? filteredSearchedItems.map((item) => {
                                    return(
                                        <li key={item.id} onClick={() => this.props.addConcernSymptom(parentId, item.id)}>
                                            {item.name?item.name:item.text}
                                        </li>
                                    )
                                }):(<li className="not-found">Elements are not found</li>)
                            }
                            </CustomScroll>
                        </ul>
                    </div>
                </div>
                <div className="bottom">
                    <h4>Connceted symptoms</h4>
                    <CustomScroll heightRelativeToParent="100%">
                        <SortableItems items={items} changeOrder={this.changeOrder} deletedItems={ forRemove[parentId] === undefined ? {} : forRemove[parentId] } cancelRemove={(childId) => this.cancelRemove(parentId, childId)} removeConcernSymptom={(childId) => this.tryRemove(parentId, childId)}/>
                    </CustomScroll>
                </div>
            </div>
                    
        );
	}
}

export default withRouter(connect(
    (state, ownProps) => ({
        store: state,
        concernsSearched: state.concerns.filter(item => item.name.includes(state.search.concern)),
        symptomsSearched: state.symptoms.filter(item => item.text.includes(state.search.symptom)),
		ownProps
    }),
    dispatch => ({
        onFindConcerns: (name) => {
            dispatch(findConcerns(name));
        },
        onFindSymptoms: (name) => {
            dispatch(findSymptoms(name));
        },
        removeConcernSymptom: (parentId, childId) => {
			dispatch(removeConcernSymptom(parentId, childId));
        },
        removeSymptomConcern: (id) => {
			dispatch(removeSymptomConcern(id));
        },
        addConcernSymptom: (parentId, childId) => {
            dispatch(addConcernSymptom(parentId, childId));
        },
        changeOrderConcernSymptoms: (concernId, orders) => {
            dispatch(changeOrderConcernSymptoms(concernId, orders));
        }
    })
)(BlockSiblings));
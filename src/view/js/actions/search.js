export const findConcerns = (name) => {
    return {
        type: 'FIND_CONCERN',
        payload: name
    };
}
export const findSymptoms = (name) => {
    return {
        type: 'FIND_SYMPTOM',
        payload: name
    };
}
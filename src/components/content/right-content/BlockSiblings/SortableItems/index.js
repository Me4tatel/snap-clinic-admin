import React, {Component} from "react";
import SortableItem from "./SortableItem";


class SortableItems extends Component {
    constructor(props){
        super(props);
        this.state = {
            items: this.props.items
        }
    }

    onSortItems = (items) => {
        this.setState({
            items: items
        });
        let orders = items.map((item)=>{
            return item.id;
        });
        this.props.changeOrder(orders);
    };
    render() {
        let { items, deletedItems } = this.props;

        var listItems = items && items.map((item, i) => (
            <SortableItem
                item={item}
                key={i}
                onSortItems={this.onSortItems}
                items={items}
                sortId={i}
                removeConcernSymptom={() => this.props.removeConcernSymptom(item.id)}
                cancelRemove={() => this.props.cancelRemove(item.id)}
                isDeleted={!!deletedItems[item.id]}
            />
        ));
        return (<ul className="sortable-list list-siblings">
            {listItems}
        </ul>);
    }
}

export default SortableItems;
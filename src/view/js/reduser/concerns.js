const ADD_CONCERN = "ADD_CONCERN";
const REMOVE_CONCERN = "REMOVE_CONCERN";
const DOWNLOAD_CONCERNS = "DOWNLOAD_CONCERNS";
const CHANGE_CONCERN = "CHANGE_CONCERN";
const DOWNLOAD_CONCERN_SYMPTOMS = "DOWNLOAD_CONCERN_SYMPTOMS";
const ADD_CONCERN_SYMPTOM = "ADD_CONCERN_SYMPTOM";
const REMOVE_CONCERN_SYMPTOM = "REMOVE_CONCERN_SYMPTOM";

export default function (state = [], action){
	switch (action.type) {
		case ADD_CONCERN:{
			let newObj = state.concat( action.payload );
			return newObj;
		}

		case REMOVE_CONCERN:{
			console.log(action.payload);
			let newObj = state.filter((item) => item.id !== action.payload.id);
			return newObj;
		}

        case CHANGE_CONCERN:{
            let newObj = state.map((item)=>{
                if(item.id === action.payload.id){
                    return{
                        ...item,
                        name: action.payload.name
                    };
                }
                return item;
            });
            return newObj;
        }

		
		case DOWNLOAD_CONCERNS:{
			let newObj = state.concat( action.payload );
			return newObj;
		}

		case DOWNLOAD_CONCERN_SYMPTOMS:{
			let newObj = state.map((item)=>{
				if(item.id === action.payload.id){
					return{
						...item,
						symptoms: action.payload.concernSymptoms
					};
				}
				return item;
			});
			return newObj;
		}

        case ADD_CONCERN_SYMPTOM:{
            let newObj = state.map((item)=>{
                if(item.id === action.payload.id){
                    return{
                        ...item,
                        symptoms: action.payload.concernSymptoms
                    };
                }
                return item;
            });
            return newObj;
        }

		case REMOVE_CONCERN_SYMPTOM:{
            let newObj = state.map((item)=>{
                if(item.id === action.payload.id){
                    return{
                        ...item,
                        symptoms: action.payload.concernSymptoms
                    };
                }
                return item;
            });
            return newObj;
		}

		default:
			return state
	}
}
const FIND_CONCERN = "FIND_CONCERN";
const FIND_SYMPTOM = "FIND_SYMPTOM";
let initialState={
	concern:'',
	symptom:''
}

export default function (state = initialState, action){
	switch (action.type) {
		case FIND_CONCERN:{
			let newState = Object.assign({}, state);
			newState.concern = action.payload;
			return newState;
		}

		case FIND_SYMPTOM:{			
			let newState = Object.assign({}, state);
			newState.symptom = action.payload;
			return newState;
		}

		default:
			return state
	}
}
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {routerMiddleware, syncHistoryWithStore} from 'react-router-redux';
import {BrowserRouter} from 'react-router-dom';


import App from './components/main';
import registerServiceWorker from './registerServiceWorker';
import allReducers from './view/js/reduser';
import browserHistory from './history';

import 'react-custom-scroll/dist/customScroll.css';
import '../src/view/css/libs.min.css';
import '../src/view/css/main.less';



const promiseMiddleware = require('redux-promise').default;
const thunkMiddleware = require('redux-thunk').default;
const store = createStore(
	allReducers, 
	composeWithDevTools(applyMiddleware(promiseMiddleware, thunkMiddleware, routerMiddleware(browserHistory)))
);

syncHistoryWithStore(browserHistory, store);



ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);


registerServiceWorker();